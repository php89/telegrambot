<?php
require_once 'vendor/autoload.php';

use App\Database\Connect;
use \App\Helper\Helper;
use TelegramBot\Api\BotApi;
use TelegramBot\Api\Types\ReplyKeyboardMarkup;

//get_declared_classes - какие классы доступны
try {
    $whiteList = [
        'Начать игру',
        'Статистика',
        'Настройки',
        'Авторизоваться в турнирах',
        'Вернуться назад',
        '1x1',
        '2x2',
        'bot',
    ];

    $data = json_decode(file_get_contents('php://input'), true);
    //467183632
    //-1001471290820
    $messageSent = $data['message']['text'];
    $user = $data['message']['from'];

    if (!in_array($messageSent, $whiteList)) {
        return false;
    }
    $helper = new Helper();
    $db = new Connect();
    $defaultKeyboard = ['Начать игру', 'Статистика', 'Настройки'];
    $chatId = $data['message']['chat']['id'];
    switch ($messageSent){
        case 'Начать игру':
            $sendMessage = 'Выберите:';
            $keyboard = ['1x1', '2x2 (в разработке)'];
            break;
        case 'Статистика':
            $sendMessage = 'Статистика' . PHP_EOL;
            foreach ($db->getStatistics() as $key => $user) {
                $sendMessage .= $key + 1 .') ' . $user['username'] . ' - ' . $user['number_points'] . PHP_EOL;
            }
            break;
        case 'Настройки':
            $sendMessage = 'Выберите действие';
            $keyboard = ['Вернуться назад'];
            if (!$db->getUserByIdTelegram($user['id'])) {
                $keyboard = ['Авторизоваться в турнирах'];
            }
            break;
        case 'Авторизоваться в турнирах':
            $db->addUser([$user['id'], $user['first_name'], $user['last_name'], $user['username']]);
            $sendMessage = '@' . $user['username'] . ' успешно добавлен';
            break;
        case '2x2':
            $sendMessage = 'В разработке';
            break;
        case '1x1':
            $sendMessage = 'Игрок @' . $user['username'] . ' создал игру.';
            break;
        default:
            $sendMessage = 'Пожалуйста, выберите действие!';
    }

    file_put_contents('test.txt', serialize(json_decode($data, true)));
    $token = "1071392811:AAHFL_UcvXm0wvxsxIzAYBT0ezLDhWyKMQE";
    $bot = new BotApi($token);

    $keyboard = new ReplyKeyboardMarkup($keyboard ? [$keyboard] : [$defaultKeyboard], true);
    $bot->sendMessage($chatId, $sendMessage, null, false, null, $keyboard);

} catch (\Exception $e) {
    file_put_contents('error.txt', $e->getMessage());
} catch (\Throwable $exception) {
    file_put_contents('error2.txt', $exception->getMessage());
}
