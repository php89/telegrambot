<?php

namespace App\Database;

use App\Helper\Helper;
use Exception;
use PDO;

//http://phpfaq.ru/pdo

/**
 * Class Connect
 * @package App\Database
 */
class Connect
{
    /**
     * @var PDO
     */
    protected PDO $pdo;

    /**
     * Connect constructor.
     * @throws Exception
     */
    public function __construct()
    {
        $config = Helper::getConfig('database');
        if (empty($config)) {
            throw new Exception('Not connection db');
        }
        $opt = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];
        $dsn = "{$config['driver']}:host={$config['host']};port={$config['port']};dbname={$config['dbname']}";
        $this->pdo = new PDO($dsn, $config['user'], $config['password'], $opt);
    }

    /**
     * @param int $idTelegram
     * @return bool
     */
    public function getUserByIdTelegram(int $idTelegram): bool
    {
        $sql = 'SELECT id FROM users WHERE id_telegram IN (?)';
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute([$idTelegram]);
        return $stmt->rowCount();
    }

    public function addUser($data)
    {
        $sql = 'INSERT INTO users (`id_telegram`, `first_name`, `last_name`, `username`) VALUES (?, ?, ?, ?)';
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($data);
    }

    /**
     * @return array
     */
    public function getStatistics(): array
    {
        $stmt = $this->pdo->query('SELECT username, number_points FROM users');
        $usersStatistics = [];
        foreach ($stmt as $row) {
            $usersStatistics[] = [
                'username' =>  $row['username'],
                'number_points' =>  $row['number_points']
            ];
        }
        return $usersStatistics;
    }
}