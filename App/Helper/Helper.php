<?php

namespace App\Helper;

class Helper
{
    /**
     * @var false|string
     */
    private static $root;
    /**
     * @var array
     */
    protected static array $config = [];

    /**
     * Helper constructor.
     */
    public function __construct()
    {
        self::$root = getcwd();
        $configPath = self::$root . "/config/config.php";
        self::$config = include $configPath;
    }

    /**
     * @param string $key
     * @return array|mixed
     */
    public static function getConfig(string $key): array
    {
        $config = self::$config;
        if (array_key_exists($key, $config)) {
            return $config[$key];
        }
        return [];
    }
}